import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native';
import { Switch } from 'react-native-switch';

export default class App extends React.Component {
  render() {
    return (

      <View style={styles.container}>
      <Button
       // onPress={onPressLearnMore}
      title="Learn More"
  color="#841584"
  accessibilityLabel="Learn more about this purple button"
/>

  <Switch
    value={true}
    onValueChange={(val) => console.log(val)}
    disabled={false}
    activeText={'On'}
    inActiveText={'Off'}
    circleSize={30}
    barHeight={1}
    circleBorderWidth={3}
    backgroundActive={'green'}
    backgroundInactive={'gray'}
    circleActiveColor={'#30a566'}
    circleInActiveColor={'#000000'}
    changeValueImmediately={true}

  />

        <Text>Raghav Puri is my name!</Text>
        <Text>Piyush go to hell!!</Text>
        <Text>Shake your phone to open the developer menu.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
